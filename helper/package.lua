return {
	name = 'SovietKitsune/helper-lib',
	version = '0.5.1',
	homepage = 'https://helper.soviet.solutions',
	dependencies = {
        'cyrilis/luvit-mongodb@0.0.24',
        'SinisterRectus/discordia@2.8.3'
	},
	tags = {'discord', 'helper'},
	license = 'GNU GPL v3.0',
	author = 'Soviet Kitsune',
	files = {'**.lua', '**.env'},
}
