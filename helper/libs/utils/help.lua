local class = require('discordia').class
local embed = client.import('embed')

local command = client.command('help', function(msg, args)
    local Embed = embed()
    Embed:setTitle('Help')

    if #args > 0 then
        local found
        for _,v in pairs(client.commands) do
            if args[1]:lower() == v.name or table.search(v.aliases, args[1]:lower()) then
                local aliases = ''

                for _,v in pairs(v.aliases) do
                    aliases = aliases .. ', `' .. v .. '`'
                end

                if aliases ~= '' then
                    aliases = aliases:sub(2,aliases:len())
                else
                    aliases = 'none'
                end

                local usage = v.name .. ' ' .. v.usage

                Embed:setDescription(
                    'Name: ' .. v.name ..
                    '\nDescription: ' .. v.description ..
                    '\nAliases: ' .. aliases ..
                    '\nUsage: ' .. usage
                )

                found = true
                break
            end
        end

        if not found then
            return msg.channel:send('That command does\'t exist')
        end
    else
        local cogs = client.cogs
        for i,v in pairs(cogs) do
            local commands = ''

            for _,command in pairs(v.commands) do
                if type(command) == 'table' then

                    if class.isObject(command) then
                        if class.type(command) == 'Command' then
                            if not command.hidden then
                                if not command.ownerOnly or msg.author.id == client.owner then
                                    commands = commands .. ',`' .. command.name .. '`'
                                end
                            end
                        end
                    end
                end
            end

            if commands ~= '' then
                commands = commands:sub(2,commands:len())

                Embed:addField(i .. ' cog', commands)
            end
        end
    end

    msg.channel:send(Embed:getTable())
end)

command:add()