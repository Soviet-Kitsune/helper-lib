local discordia = require('discordia')
local enums = discordia.enums
local class = discordia.class

local Error = client.import('error')
local array = client.import('array')

local timer = require('timer')

local checks = {}

checks.errors = {
    permissions = 'You do not have correct permissions! Needed: %s',
    guild = 'You are not in a guild!',
    role = 'You do not have the correct role! Needed: %s',
    owner = false, -- sneak 100
    guildOwner = 'Only the guild owner can do this!',
    cooldown = 'You are on cooldown!'
}

--TODO Cooldown stuff --

local cooldowns = array()

function checks.cooldown(msg, v)
    local has = cooldowns:find(function(id)
        return id.id == msg.author.id and id.cooldown == v.name
    end)

    assert(has == nil, Error('cooldown'))

    local pos = cooldowns:push({
        id = msg.author.id,
        cooldown = v.name
    })

    timer.setTimeout(v.time, function()
        cooldowns:pop(pos)
    end)
end

-- Is --

function checks.isGuild(msg)
    assert(msg.guild, Error('guild'))

    return msg.guild
end

function checks.isGuildOwner(msg)
    assert(checks.isGuild(msg), Error('guild'))

    assert(msg.member.id == msg.guild.owner.id, Error('guildOwner'))

    return msg.member.id == msg.guild.owner.id
end

function checks.isBotOwner(msg)
    assert(table.search(client.owners, msg.author.id), Error('owner'))

    return table.search(client.owners, msg.author.id)
end

-- Has --

function checks.hasRole(msg,role)
    assert(checks.isGuild(msg), Error('guild'))

    if msg.member:hasRole(role) then
        return true
    else
        local has = msg.member.roles:find(function(role)
            return role.name:lower() == role
        end)

        assert(has, Error('role'))

        return has
    end
end

function checks.hasPermission(msg, permission)
    assert(checks.isGuild(msg), Error('guild'))

    local channel = msg.channel

    assert(msg.member:hasPermission(channel, enums.permission[permission]), Error('permissions'))

    return msg.member:hasPermission(channel, enums.permission[permission])
end

function checks.hasPermissions(msg,Perms)
    assert(checks.isGuild(msg), Error('guild'))

    if #Perms == 1 then
        return checks.hasPermission(msg,Perms[1])
    end

    local perms = msg.member:getPermissions(msg.channel)

    assert(perms:has(unpack(Perms)), Error('permissions'))

    return perms:has(unpack(Perms))
end

-- Non checks

function checks.add(name,func, msg, errorName)
    client.check(name, 'Name not specified')
    client.check(func, 'Function not specified')

    checks[name] = func

    if not msg then msg = false end
    if not errorName then errorName = name end

    checks.errors[errorName] = msg
end

function checks.parse(config)
    return function(msg)
        for i,v in pairs(config) do
            p(i)
            local succ,err = pcall(function()
                checks[i](msg,v)
            end)

            if not succ and class.isObject(err) then
                err.type = err.reason
                err.value = v

                return false, err
            end
        end

        return true
    end
end

return checks