local Mongo = require('luvit-mongodb')
local class = require('discordia').class

local db = {}
local schema, schemaGet = class('Mongodb Schema')
local model, modelGet = class('Mongodb Model')

if not client then client = require('../../init.lua') end

local Promise = client.import('promise')
local array = client.import('array')
local Error = client.import('error')

Promise.async = function(cb)
    require('timer').setInterval(0,cb)
end

local queue = array()

function fixData(db,name,data)
    function data.save()
        local promise = Promise.new()
        db.db:update(name,{_id = data.clean()._id}, {['$set'] = data.clean()},false,nil, function(err,res)
            if err then
                promise:reject(err)
            else
                promise:resolve(res)
            end
        end)

        return promise
    end

    function data.delete()
        local promise = Promise.new()
        db.db:remove(name,{_id = data.clean()._id}, function(err,res)
            if err then
                promise:reject(err)
            else
                promise:resolve(res)
            end
        end)

        return promise
    end

    function data.clean()
        local cleaned = {}
        for i,k in pairs(data) do
            if type(k) ~= 'function' then
                cleaned[i] = k
            end
        end

        return cleaned
    end

    return data
end

function db:connect(config, cb)
    db.db = Mongo:new(config)
    db.schemas = array()

    db.connected = false

    db.db:on('connect', function()
        db.connected = true

        queue:forEach(function(func)
            func(db.db)
        end)
        if cb then
            cb()
        end
    end)
end

function db:getSchema(name)
    return db.schemas:find(function(k)
        return k.name == name
    end)
end

function db:schema(Schema)
    local scheme = schema(db,Schema)
    db.schemas:push(scheme)

    return scheme
end

function db:model(name, schema)
    schema._name = name

    schema._collection = db.db:collection(name)

    local customClass = class(name .. ' Model')

    function customClass:__init(Model, noMake)
        model.__init(self, Model, schema, noMake)
    end

    function customClass:save()

    end

    function customClass.findAll(query)
        local promise = Promise.new()

        local function check()
            schema._collection:find(query, function(err,res)
                if err then
                    promise:reject(err)
                else
                    local models = array()
                    for _,v in pairs(res) do
                        v = fixData(schema._db, schema._name, v)
                        models:push(v)
                    end
                    promise:resolve(models)
                end
            end)
        end

        if db.connected then
            check()
        else
            queue:push(check)
        end

        return promise
    end

    function customClass.findOne(query)
        local promise = Promise.new()

        customClass.findAll(query):next(function(res)
            promise:resolve(res:toTable()[1])
        end, function(err)
            promise:reject(err)
        end)

        return promise
    end

    return customClass
end

function model:__init(Model, schema, noMake)
    local promise = Promise.new()

    self._schema = schema
    local model = self._schema:check(Model)

    -- Upload to mongo

    if model and not noMake then
        local function make()
            self._schema._collection:insert(model, function(err,res)
                if err then
                    promise:reject(err)
                else
                    data = fixData(self._schema._db, self._schema._name, data)
                    promise:resolve(data)
                end
            end)
        end

        if db.connected then
            make()
        else
            queue:push(make)
        end
    end

    self._model = model

    return model
end

function schema:__init(db,schema)
    self._db = db

    for i,v in pairs(schema) do
        if type(v) == 'string' then
            schema[i] = {type = v}
        end
    end

    self._schema = schema
end

function schema:check(model)
    for i,v in pairs(self._schema) do
        if not model[i] then
            if v.default then
                model[i] = v.default
            else
                Error('Key: ' .. i .. ' was not defined and no default was found'):log()
                return false
            end
        end
    end

    return model
end

function schemaGet.name(self)
    return self._name
end

-- Test --
--[[
db:connect({db = 'testDb'})

local user = db:schema({
    username = 'string',
    password = 'string',
    illegal = {type = 'string', default = true}
})

user = db:model('User', user)

user.findOne({
    username = 'soviet'
}):next(function(res)
    res.password = 'shit'

    res.save()
end,
function(err)
    p(err)
end)
--]]

return db