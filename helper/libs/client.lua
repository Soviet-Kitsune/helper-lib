local discordia = require('discordia')

local class = discordia.class

local helper = require('../init')
local import = helper.import

local Client, get = class('Helper Client', discordia.Client)

local array = import('array')

local Error = import('error')

local checks = import('checks')

function Client:__init(prefix,token,config)
    discordia.Client.__init(self)

    if not config then config = {} end

    self:info('Soviet Helper ' .. helper.version)

    self._prefix = prefix
    self._token = token
    self._config = config

    self._owners = config.owners or {config.owner} or nil

    self._ready = false

    self._commands = array()
    self._cogs = array()

    self._queue = array() -- queue of things to do once ready

    _G.client = self

    self:on('messageCreate', function(msg)
        if not string.startswith(msg.content,self._prefix) then return end

        local Command = string.split(msg.content, ' ')

        Command = string.gsub(Command[1],self._prefix, '')

        local args = string.split(msg.content, ' ')
        args = table.slice(args, 2)

        Command = string.lower(Command)

        for _,v in pairs(self._commands) do
            if v then
                if v:check(Command,msg) then
                    msg.channel:broadcastTyping()
                    v:run(Command,msg,args)
                    break
                end
            end
        end
    end)

    self:on('ready', function()
        self:info('Ready as ' .. self.user.username)

        -- Don't load anything until the bot is ready
        -- This is so then things the user wants get loaded in time

        if config.useDefaultHelp then
            import('defaultHelp')
        end
    
        if config.database then
            -- TODO database

            local db = import('mongodb')

            db:connect(config.database)

            self._db = db
        end

        self._ready = true

        self._queue:forEach(function(func) func() end)

        self._queue = nil
    end)
end

function Client:connect(status)
    self:run('Bot ' .. self._token)
    if status then
        self:setGame(status)
    end
end

function Client:getCog(name)
    return self._cogs:find(function(k)
        return class.type(k) == name
    end)
end

function Client:addCog(cog)
    self._cogs:push(cog)
end

function Client:addNewCommands(cog)
    local function add()
        for _,v in pairs(cog.commands) do
            if not self._commands:find(function(k) return k.name == v.name end) then
                self._commands:push(v)
            end
        end
    end

    if not self._ready then
        self._queue:push(add)
    else
        add()
    end
end

function Client.check(statement,message)
    if not statement then
        Error(message):log()
    end
end

function Client.addCheck(...)
    checks.add(...)
end

function get.command()
    return require('./classes/command')
end

function get.cog()
    return require('./classes/cog')
end

function get.import()
    return import
end

function get.prefix(self)
    return self._prefix
end

function get.commands(self)
    return self._commands
end

function get.owners(self)
    return self._owners
end

function get.cogs(self)
    return self._cogs
end

return Client