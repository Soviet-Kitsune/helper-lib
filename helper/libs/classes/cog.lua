local discordia = require('discordia')
local class = discordia.class

local cog, get = class('Cog')

function cog:__init()
    self._commands = {}
end

function cog:addCommand(command, func, data)
    if class.isObject(command) then
        command.category = class.type(self)
        table.insert(self._commands, command)
    else
        local Command = client.command(command, func,data)

        Command.category = class.type(self)
        table.insert(self._commands, Command)
    end

    client:addNewCommands(self)
end

function cog:on(event,cb)
    client:on(event,cb)
end

function cog:log(level, ...)
    if level == 'info' or level == 'success' then
        client:info(...)
    elseif level == 'warning' then
        client:warn(...)
    elseif level == 'debug' then
        client:debug(...)
    elseif level == 'error' then
        client:error(...)
    end
end

function get.command()
    return require('./command')
end

function get.commands(self)
    return self._commands
end

return cog