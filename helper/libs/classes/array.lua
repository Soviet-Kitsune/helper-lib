local discordia = require('discordia')
local class = discordia.class

local array = class('Array')

function array:__init(...)
    self._data = {...}
end

function array:__len()
    return #self._data
end

function array:__pairs()
    return function(tbl,k)
        local v

        k,v = next(tbl, k)

        if v then
            return k,v
        end
    end, self._data, nil
end

function array:toTable()
    return self._data
end

function array:find(func)
    for _,v in pairs(self) do
        if func(v) then
            return v
        end
    end
end

function array:findAll(func)
    local data = {}

    for _,v in pairs(self) do
        if func(v) then
            table.insert(data, v)
        end
    end

    return array(unpack(data))
end

function array:forEach(func)
    for i,v in pairs(self) do
        func(v,i,self._data)
    end
end

function array:slice(start,stop,step)
    return table.slice(self._data, start,stop,step)
end

function array:push(item)
    table.insert(self._data, item)

    return #self._data
end

function array:shift()
    table.remove(self._data, 1)
end

function array:pop(pos)
    table.remove(self._data, pos)
end

return array