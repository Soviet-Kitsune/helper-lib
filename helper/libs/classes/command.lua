local discordia = require('discordia')
local class = discordia.class

local command, get, set = class('Command')

local checks = client.import('checks')

local errors = checks.errors

function command:__init(name, func, config)
    assert(name, 'No function/name set')
    assert(func, 'No function set')

    if type(name) == 'function' then
        config = func
        func = name
        name = 'placeholder'
    end

    self._name = name
    self._run = func

    local exclude = {
        'desc', 'description', 'alias',
        'aliases', 'usage'
    }

    if not config then config = {} end

    self._desc = config.desc or config.description or 'none'
    self._aliases = config.aliases or {config.alias} or {}
    self._usage = config.usage or ''
    self._category = config.category or 'Misc'

    local Checks = {}

    for i,v in pairs(config) do
        if not table.search(exclude, i) then
            Checks[i] = v
        end
    end

    self._checks = checks.parse(Checks) -- Returns a function
end

function command:add()
    local cog = client:getCog('Misc')

    if not cog then
        local cogClass = require('./cog')
        local Misc = class('Misc', cogClass)

        function Misc:__init()
            cogClass.__init(self)
        end
        cog = Misc()

        client:addCog(cog)
    end

    cog:addCommand(self)
end

function command:check(command,msg)
    local check = command:lower() == self._name or table.search(self._aliases, command:lower())

    if not check then return false end

    local succ, err = self._checks(msg)

    if not succ then
        local value = err.value

        if errors[err.type] then
            if type(value) == 'table' then
                value = table.concat(value,',')
            end

            local parsedError = string.format(errors[err.type], err.value)

            if parsedError then
                if client:getListenerCount(err.type .. ' error') == 0 then
                    msg.channel:send(parsedError)
                else
                    client:emit(err.type .. ' error', msg, err)
                end
            end
        end

        return false
    end

    return check
end

function command:run(msg,args)
    self._run(msg,args)
end

function get.name(self)
    return self._name
end

function get.category(self)
    return self._category
end

function get.aliases(self)
    return self._aliases
end

function get.description(self)
    return self._desc
end

function get.usage(self)
    return self._usage
end

function set.name(self, name)
    self._name = name
end

function set.category(self, category)
    self._category = category
end

return command