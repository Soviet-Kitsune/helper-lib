local discordia = require('discordia')
local embed = discordia.class('Embed')

function embed:__init()
    self._embed = {}
    self._noIndex = {}
    self._colors = {
        lightRed = 0xff7675,
        red = 0xd63031,
        lightBlue = 0x74b9ff,
        blue = 0x0984e3,
        lightGreen = 0x55efc4,
        green = 0x00b894,
        lightPurple = 0xa29bfe,
        purple = 0x6c5ce7,
        lightYellow = 0xffeaa7,
        yellow = 0xfdcb6e,
        lightOrange = 0xfab1a0,
        orange = 0xe17055,
        lightGray = 0xdfe6e9,
        gray = 0xb2bec3,
        darkGrey = 0x636e72,
        lightBlack = 0x2d3436,
        lightPink = 0xfd79a8,
        pink = 0xe84393,
        lightTeal = 0x81ecec,
        teal = 0x00cec9
    }

    for _,v in pairs(self._colors) do
        table.insert(self._noIndex, v)
    end
end

function embed:setTitle(title)
    self._embed.title = title
    return self
end

function embed:setDescription(desc)
    self._embed.description = desc
    return self
end

function embed:setColor(color)
    self._embed.color = color
    return self
end

function embed:addField(name,value, inline)
    if not inline then inline = false end

    if not self._embed.fields then self._embed.fields = {} end

    table.insert(self._embed.fields, {name = name, value = value, inline = inline})

    return self
end

function embed:setAuthor(name, icon, url)
    if not icon then icon = "" end if not url then url = "" end

    self._embed.author = {
        name = name,
        icon_url = icon,
        url = url
    }

    return self
end

function embed:setFooter(text, icon)
    if not icon then icon = "" end

    self._embed.footer = {
        text = text,
        icon_url = icon
    }

    return self
end

function embed:setImage(img)
    self._embed.image = {
        url = img
    }

    return self
end

function embed:setThumbnail(url)
    self._embed.thumbnail = {
        url = url
    }

    return self
end

function embed:setTimestamp(date)
    if not date then date = os.date("!%Y-%m-%dT%TZ") end
    self._embed.timestamp = date

    return self
end

function embed:setURL(url)
    self._embed.url = url

    return self
end

function embed:getTable()
    if not self._embed['color'] then
        local color = self._noIndex[math.random(1,#self._noIndex)]
        self:setColor(color)
    end
    return {embed = self._embed}
end

function embed:getRaw()
    if not self._embed['color'] then
        local color = self._noIndex[math.random(1,#self._noIndex)]
        self:setColor(color)
    end
    return self._embed
end

return embed