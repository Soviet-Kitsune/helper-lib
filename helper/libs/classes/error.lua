local discordia = require('discordia')
local class = discordia.class

local Error, get,set = class('Error')

function Error:__init(reason, other)
    self._traceback = debug.traceback()
    self._reason = reason

    if other then
        self._type = other.type or 'undefined'
        self._value = other.value or 'undefined'
    end
end

function Error:__tostring()
    return 'Error: ' .. self:tostring()
end

function Error:tostring()
    return self._reason .. '\n' .. (self._traceback or '')
end

function Error:log()
    if not client then
        return error('Unhandled error: ' .. self:tostring())
    end

    if client:getListenerCount('error') == 0 then
        error('Unhandled error: ' .. self:tostring())
    else
        client:emit('error', self:tostring())
    end
end

function get.reason(self)
    return self._reason
end

function set.reason(self, reason)
    self._reason = reason
end

function get.type(self)
    return self._type
end

function set.type(self, Type)
    self._type = Type
end

function get.value(self)
    return self._value
end

function set.value(self, value)
    self._value = value
end

return Error