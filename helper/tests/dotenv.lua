require('../testing')

local env = import('dotenv')

describe('Dotenv', function(test)
    test('Parse string', function(finish)
        local data = env.parse('AFTER_LINE=after_line\nEMPTY=')
        local expecting = { EMPTY = '', AFTER_LINE = 'after_line' }

        for i,v in pairs(expecting) do
            assert(data[i] == v,'Values don\'t match')
        end

        finish()
    end)

    test('Parse file',function(finish)
        local data = env.config({
            path = './test.env'
        })

        data = data[1]

        local expecting = {
            BASIC = 'basic',
            AFTER_LINE = 'after_line',
            EMPTY = '',
            SINGLE_QUOTES = 'single_quotes',
            DOUBLE_QUOTES= 'double_quotes',
            RETAIN_INNER_QUOTES= '{"foo": "bar"}',
            RETAIN_INNER_QUOTES_AS_STRING= '{"foo": "bar"}',
            TRIM_SPACE_FROM_UNQUOTED= 'some spaced out string',
            USERNAME= 'therealnerdybeast@example.tld',
            SPACED_KEY= 'parsed'
        }

        if #data ~= #expecting then
            finish:reject('Data is missing or more then expected')
        end

        for i,v in pairs(expecting) do
            assert(data[i] == v,'Values don\'t match;' .. i .. ' Expected: ' .. v .. ' Got: ' .. data[i])
        end

        finish()
    end)
end)