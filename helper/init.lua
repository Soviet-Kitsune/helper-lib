local helper = {}

local Error = require('./libs/classes/error')

require('./libs/extenstions')()

local locations = {
    'classes/', 'utils/', 'schemas/',
    ''
}
-- TODO make better autodoc
helper.version = '0.5-Dev'

-- TODO tests

local mods = {}

function helper.import(file)
    if mods[file] then return mods[file] end

    local import
    for _,v in pairs(locations) do
        local succ,err = pcall(require, './libs/' .. v .. file)
        if succ then
            import = require('./libs/' .. v .. '/' .. file)
            break
        end
    end

    if import then
        mods[file] = import
        return import
    else
        return Error(file .. ' was not found!\n'):log()
    end
end

_G.client = helper -- Let modules import there tools

helper.require = helper.import

return helper