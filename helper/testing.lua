local helper = require('./init')

local timer = require('timer')
local fs = require('fs')
local json = require('json')
local Promise = helper.import('promise')

Promise.async = function(callback)
    timer.setInterval(0, callback)
end

local testing = {}

testing.queue = {}
testing.promise = Promise

function getSuccess()
    if process.argv[2] and process.argv[2] == 'shell' then
        return ''
    else
        return '\27[38;5;46m\27[48;5;22m'
    end
end

function getFailure()
    if process.argv[2] and process.argv[2] == 'shell' then
        return ''
    else
        return '\27[38;5;208m\27[48;5;52m'
    end
end

function getBackground()
    if process.argv[2] and process.argv[2] == 'shell' then
        return ''
    else
        return '\27[1;36m\27[48;5;235m'
    end
end

function getNormal()
    if process.argv[2] and process.argv[2] == 'shell' then
        return ''
    else
        return '\27[1;0m'
    end
end

function testing.deepcompare(t1,t2,ignore_mt)
    local ty1 = type(t1)
    local ty2 = type(t2)
    
    if ty1 ~= ty2 then return false end
    -- non-table types can be directly compared
    if ty1 ~= 'table' and ty2 ~= 'table' then return t1 == t2 end
    -- as well as tables which have the metamethod __eq
    local mt = getmetatable(t1)

    if not ignore_mt and mt and mt.__eq then return t1 == t2 end

    for k1,v1 in pairs(t1) do
        local v2 = t2[k1]
        if v2 == nil or not testing.deepcompare(v1,v2) then return false end
    end
    for k2,v2 in pairs(t2) do
        local v1 = t1[k2]
        if v1 == nil or not testing.deepcompare(v1,v2) then return false end
    end
    return true
end

function testing.test(sit,func)
    table.insert(testing.queue, {sit,func})
end

function testing.start(Sit,Func)
    local _,err = pcall(Func, testing.test)

    if err then
        print('Error: failed to start testing; ' .. err)
        process:exit()
    end

    _G.realAssert = assert

    print('## Starting: ' .. Sit)

    local passed = 0

    local count = 0

    local passes = {}
    local fails = {}

    local function loop()
        count = count+1

        if count == #testing.queue+1 then
            print(Sit .. ' passed: ' .. passed .. '/' .. #testing.queue .. ' tests')

            if process.argv[3] then
                if fs.existsSync(process.argv[3]) then
                    local testData = fs.readFileSync(process.argv[3])

                    testData = json.decode(testData)

                    testData[Sit] = {}

                    testData[Sit].Passes = passes
                    testData[Sit].Fails = fails

                    testData.TotalPass = passed + testData.TotalPass
                    testData.TotalFails = math.abs((passed - #testing.queue)) + testData.TotalFails
                    testData.TotalTests = testData.TotalTests + #testing.queue

                    fs.writeFileSync(process.argv[3], json.encode(testData))
                else
                    local testData = {}

                    testData[Sit] = {}

                    testData[Sit].Passes = passes
                    testData[Sit].Fails = fails

                    testData.TotalPass = passed
                    testData.TotalFails = math.abs(passed - #testing.queue)
                    testData.TotalTests = #testing.queue

                    fs.writeFileSync(process.argv[3], json.encode(testData))
                end
            end

            testing.queue = {}

            print()
        end

        local sit,func = unpack(testing.queue[count])

        print('### Starting Test: ' .. getBackground() .. sit .. getNormal())

        local promise = Promise.new()

        _G.assert = function(statement, err)
            if not statement then
                promise:reject(err)
            end
        end

        promise.mt.__call = function(self)
            promise.resolve(self, true)
        end

        xpcall(function()
            func(promise)
        end, function(err)
            promise:reject(err)
        end)

        promise:next(function()
            passed = passed + 1
            print('Passed: ' .. getSuccess() .. sit .. getNormal())
            table.insert(passes, sit)
            loop()
        end, function(Err)
            print('Failed: ' .. getFailure() .. sit ..getNormal().. '\n' .. Err)
            table.insert(fails, sit)
            loop()
        end)

        print()
    end

    loop()
end

_G.describe = testing.start
_G.import = helper.import

return testing