# Helper-lib DEV

<p align="center">
    <img src="https://img.shields.io/endpoint?url=https%3A%2F%2Fbitbucket.org%2FSoviet-Kitsune%2Fhelper-lib%2Fraw%2F11144a59f88477932aa4b11743ff841f47b5e53e%2Fshield.json">
</p>


Currently a work in progress framework. More details to come.

This is the development branch meaning bugs!

To run tests you need to use `./scripts/runtests.sh`. This will return a json output that would need to be cleaned. To auto clean it use `./scripts/runtests.sh clean` which requires [prettier]('https://prettier.io/') CLI to be installed.

## Todo

* [ ] Cooldown stuff with databases (Only if its longer then 10 minutes)
* [ ] More database integration
* [ ] Auto-documentation
* [ ] Tests
* [x] Better folder names