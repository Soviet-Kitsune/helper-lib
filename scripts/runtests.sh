#!/bin/bash

prevDir=$(echo $PWD)

if test -f "$prevDir/res.json"; then
    rm "$prevDir/res.json"
fi

if test -f "$prevDir/shield.json"; then
    rm "$prevDir/shield.json"
fi

cd ./helper/tests

for f in *.lua
do
	luvit "$f" user res.json
done

mv ./res.json "$prevDir"

cd "$prevDir"

if [ "$1" = "clean" ]; then # Assume you have npm installed
    prettier --write ./res.json >&-
fi

if [ "$1" = "shield" ] || [ "$2" = "shield" ] ; then # Generate json for shields.io
    echo "local a=require('json')local b=require('fs')local c=b.readFileSync('./res.json')c=a.decode(c)local d={}d.schemaVersion=1;d.label='Tests'local e=c.TotalPass/c.TotalTests;d.message=e*100 ..'% passing'function getColor(f)f=f*100;if f>80 then return'00ff00'elseif f>60 then return'ffff00'elseif f>40 then return'ff8000'elseif f>20 then return'ff5500'else return'ff0000'end end;d.color=getColor(e)d.style='for-the-badge';b.writeFileSync('shield.json',a.encode(d))" > run.lua
    luvit run.lua

    rm -r run.lua
fi

echo -e "\nTesting finished, check output: $prevDir/res.json"