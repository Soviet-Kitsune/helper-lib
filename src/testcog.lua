local discordia = require('discordia')
local class = discordia.class

local Information = class('Information', client.cog)

function Information:__init()
    client.cog.__init(self)

    self:addCommand('ping', function(msg)
        msg.channel:send('pong')
    end, {
        cooldown = {
            time = 10000,
            name = 'ping'
        }
    })
end

client:addCog(Information())