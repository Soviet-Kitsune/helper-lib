local discordia = require('discordia')

local helper = require('../helper')

local client = helper.import('client')

helper.import('dotenv').config()

client = client(';', process.env.TOKEN, {
    owner = {'525840152103223338'},
    useDefaultHelp = true,
    database = {
        db = 'test-database'
    }
})

client:on('error', function(err) -- Error handling
    print(err)
end)

require('./testcog')
--]]

client:connect()